defmodule Identicon.Image do
  defstruct hex: nil, color: nil, grid: nil, pixel_map: nil
end

defmodule Identicon do
  def main(input) do
    input
    |> hash_input
    |> pick_color
    |> build_grid
    |> build_pixel_map
    |> draw_image
    |> save_image(input)
  end

  def hash_input(input) do
    hex = :crypto.hash(:md5, input)
    |> :binary.bin_to_list

    %Identicon.Image{hex: hex}
  end

  # Return an [r,g,b] list
  def pick_color(image) do
    [r, g, b | _tail] = image.hex

    %Identicon.Image{image | color: {r, g, b}}
  end

  def build_grid(image) do
    grid =
      image.hex
      |> Enum.chunk_every(3, 3, :discard) # chunk 3 at a time, discard if less than 3 remains
      |> Enum.map(&mirror_row/1)
      |> List.flatten
      |> Enum.with_index
      |> Enum.filter(fn({code, _index}) -> rem(code, 2) == 0 end)

    %Identicon.Image{image | grid: grid}
  end

  def mirror_row([first, second, third]) do
    [first, second, third, second, first]
  end

  def build_pixel_map(image) do
    pixel_map = Enum.map image.grid, fn({_code, index}) ->
      x = rem(index, 5) * 50
      y = div(index, 5) * 50

      top_left = {x, y}
      bot_right = {x + 50, y + 50}

      {top_left, bot_right}
    end

    %Identicon.Image{image | pixel_map: pixel_map}
  end

  def draw_image(image) do
    drawn_image = :egd.create(250, 250)
    fill = :egd.color(image.color)

    Enum.each image.pixel_map, fn({start, stop}) ->
      :egd.filledRectangle(drawn_image, start, stop, fill)
    end

    :egd.render(drawn_image)
  end

  def save_image(file, input) do
    File.write("#{input}.png", file)
  end
end
